USE [CreditPlatform]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 2/16/2022 12:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AccountNumber] [varchar](25) NOT NULL,
	[CurrentBalance] [float] NOT NULL,
	[ClientId] [int] NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Clients]    Script Date: 2/16/2022 12:58:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clients](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](25) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[IdentificationNumber] [varchar](25) NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Clients] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Movements]    Script Date: 2/16/2022 12:58:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Movements](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Amount] [float] NOT NULL,
	[AccountId] [int] NOT NULL,
	[CreatedAt] [datetime] NOT NULL,
	[UpdatedAt] [datetime] NOT NULL,
	[TypeMovementId] [int] NOT NULL,
 CONSTRAINT [PK_Movements] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TypesMovements]    Script Date: 2/16/2022 12:58:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TypesMovements](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](20) NOT NULL,
	[Value] [int] NOT NULL,
	[CreatedAt] [datetime] NOT NULL,
	[UpdatedAt] [datetime] NOT NULL,
 CONSTRAINT [PK_TypesMovements] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Account] ON 

INSERT [dbo].[Account] ([Id], [AccountNumber], [CurrentBalance], [ClientId], [Active]) VALUES (1, N'142562555', 0, 1, 1)
INSERT [dbo].[Account] ([Id], [AccountNumber], [CurrentBalance], [ClientId], [Active]) VALUES (2, N'2541685', 19000, 3, 1)
SET IDENTITY_INSERT [dbo].[Account] OFF
GO
SET IDENTITY_INSERT [dbo].[Clients] ON 

INSERT [dbo].[Clients] ([Id], [Name], [LastName], [IdentificationNumber], [Active]) VALUES (1, N'Ibrahim Alexis ', N'Lopez Roman', N'12345678', 1)
INSERT [dbo].[Clients] ([Id], [Name], [LastName], [IdentificationNumber], [Active]) VALUES (2, N'Armando', N'Perez', N'748656', 0)
INSERT [dbo].[Clients] ([Id], [Name], [LastName], [IdentificationNumber], [Active]) VALUES (3, N'Jose Mario ', N'Martinez', N'854774121', 1)
SET IDENTITY_INSERT [dbo].[Clients] OFF
GO
SET IDENTITY_INSERT [dbo].[Movements] ON 

INSERT [dbo].[Movements] ([Id], [Amount], [AccountId], [CreatedAt], [UpdatedAt], [TypeMovementId]) VALUES (2, 500, 1, CAST(N'2022-02-16T11:25:14.793' AS DateTime), CAST(N'2022-02-16T11:25:14.793' AS DateTime), 1)
INSERT [dbo].[Movements] ([Id], [Amount], [AccountId], [CreatedAt], [UpdatedAt], [TypeMovementId]) VALUES (3, 2500, 1, CAST(N'2022-02-16T11:55:06.890' AS DateTime), CAST(N'2022-02-16T11:55:06.890' AS DateTime), 2)
INSERT [dbo].[Movements] ([Id], [Amount], [AccountId], [CreatedAt], [UpdatedAt], [TypeMovementId]) VALUES (4, 15000, 2, CAST(N'2022-02-16T12:28:36.317' AS DateTime), CAST(N'2022-02-16T12:28:36.317' AS DateTime), 2)
INSERT [dbo].[Movements] ([Id], [Amount], [AccountId], [CreatedAt], [UpdatedAt], [TypeMovementId]) VALUES (5, 1500, 2, CAST(N'2022-02-16T12:31:21.187' AS DateTime), CAST(N'2022-02-16T12:31:21.187' AS DateTime), 1)
INSERT [dbo].[Movements] ([Id], [Amount], [AccountId], [CreatedAt], [UpdatedAt], [TypeMovementId]) VALUES (6, 3000, 1, CAST(N'2022-02-16T12:38:38.647' AS DateTime), CAST(N'2022-02-16T12:38:38.647' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Movements] OFF
GO
SET IDENTITY_INSERT [dbo].[TypesMovements] ON 

INSERT [dbo].[TypesMovements] ([Id], [Name], [Value], [CreatedAt], [UpdatedAt]) VALUES (1, N'Withdrawal', -1, CAST(N'2022-02-16T09:59:05.113' AS DateTime), CAST(N'2022-02-16T09:59:05.113' AS DateTime))
INSERT [dbo].[TypesMovements] ([Id], [Name], [Value], [CreatedAt], [UpdatedAt]) VALUES (2, N'Deposit', 1, CAST(N'2022-02-16T09:59:05.113' AS DateTime), CAST(N'2022-02-16T09:59:05.113' AS DateTime))
SET IDENTITY_INSERT [dbo].[TypesMovements] OFF
GO
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_Active]  DEFAULT ((1)) FOR [Active]
GO
ALTER TABLE [dbo].[Clients] ADD  CONSTRAINT [DF_Clients_Active]  DEFAULT ((1)) FOR [Active]
GO
ALTER TABLE [dbo].[Movements] ADD  CONSTRAINT [DF_Movements_CreatedAt]  DEFAULT (getdate()) FOR [CreatedAt]
GO
ALTER TABLE [dbo].[Movements] ADD  CONSTRAINT [DF_Movements_UpdatedAt]  DEFAULT (getdate()) FOR [UpdatedAt]
GO
ALTER TABLE [dbo].[TypesMovements] ADD  CONSTRAINT [DF_TypesMovements_Value]  DEFAULT ((1)) FOR [Value]
GO
ALTER TABLE [dbo].[TypesMovements] ADD  CONSTRAINT [DF_TypesMovements_CreatedAt]  DEFAULT (getdate()) FOR [CreatedAt]
GO
ALTER TABLE [dbo].[TypesMovements] ADD  CONSTRAINT [DF_TypesMovements_UpdatedAt]  DEFAULT (getdate()) FOR [UpdatedAt]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Clients] FOREIGN KEY([ClientId])
REFERENCES [dbo].[Clients] ([Id])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Clients]
GO
ALTER TABLE [dbo].[Movements]  WITH CHECK ADD  CONSTRAINT [FK_Movements_Account] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Account] ([Id])
GO
ALTER TABLE [dbo].[Movements] CHECK CONSTRAINT [FK_Movements_Account]
GO
ALTER TABLE [dbo].[Movements]  WITH CHECK ADD  CONSTRAINT [FK_Movements_TypesMovements] FOREIGN KEY([TypeMovementId])
REFERENCES [dbo].[TypesMovements] ([Id])
GO
ALTER TABLE [dbo].[Movements] CHECK CONSTRAINT [FK_Movements_TypesMovements]
GO
/****** Object:  StoredProcedure [dbo].[spAccount_Add]    Script Date: 2/16/2022 12:58:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ibrahim Alexis Lopez Roman
-- Create date: 15/02/2022
-- Description:	Add new account.
-- =============================================
CREATE PROCEDURE [dbo].[spAccount_Add]
	@AccountNumber varchar(25),
	@CurrentBalance numeric,
	@ClientId int
AS
BEGIN
	INSERT INTO Account (AccountNumber, CurrentBalance, ClientId)
	VALUES(@AccountNumber, @CurrentBalance, @ClientId)
	SELECT @@IDENTITY AS Id 

END
GO
/****** Object:  StoredProcedure [dbo].[spAccount_Delete]    Script Date: 2/16/2022 12:58:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ibrahim Alexis Lopez Roman
-- Create date: 15/02/2022
-- Description:	Soft delete account
-- =============================================
CREATE PROCEDURE [dbo].[spAccount_Delete]
	@Id int
AS
BEGIN
	UPDATE Account 
	SET Active = 0 
	OUTPUT inserted.Id
	WHERE Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[spAccount_GetAll]    Script Date: 2/16/2022 12:58:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ibrahim Alexis Lopez Roman
-- Create date: 15/02/2022
-- Description:	Get all accounts records.
/*
	EXEC [dbo].[spAccount_GetAll]	
*/
-- =============================================
CREATE PROCEDURE [dbo].[spAccount_GetAll]	
AS
BEGIN
	
	
	SELECT 
	act.*, 
	cli.Id, 
	cli.Name, 
	cli.LastName,
	cli.IdentificationNumber,
	mv.*
    FROM Account act
	LEFT OUTER JOIN Movements mv on mv.AccountId = act.Id
    INNER JOIN (
		select 
		t.Id, t.Name, t.LastName, t.IdentificationNumber
        FROM (
			SELECT * FROM Clients GROUP BY Id, Name, LastName, IdentificationNumber, Active
		) t GROUP BY Id, Name, LastName, IdentificationNumber, Active) cli ON cli.Id = act.ClientId	
	WHERE act.Active = 1
	
    ORDER BY act.Id DESC
END
GO
/****** Object:  StoredProcedure [dbo].[spAccount_GetById]    Script Date: 2/16/2022 12:58:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ibrahim Alexis Lopez Roman
-- Create date: 15/02/2022
-- Description:	Get account by Id.
/*
 exec [dbo].[spAccount_GetById] 1
*/
-- =============================================
CREATE PROCEDURE [dbo].[spAccount_GetById]
	@Id int
AS
BEGIN
	
	
	SELECT 
	act.*, 
	cli.Id, 
	cli.Name, 
	cli.LastName,
	cli.IdentificationNumber,
	mv.*
    FROM Account act
	LEFT OUTER JOIN Movements mv on mv.AccountId = act.Id
    INNER JOIN (
		select 
		t.Id, t.Name, t.LastName, t.IdentificationNumber
        FROM (
			SELECT * FROM Clients GROUP BY Id, Name, LastName, IdentificationNumber, Active
		) t GROUP BY Id, Name, LastName, IdentificationNumber, Active) cli ON cli.Id = act.ClientId	
	WHERE act.Active = 1 AND act.Id = @Id
    ORDER BY act.Id DESC
END
GO
/****** Object:  StoredProcedure [dbo].[spAccount_Update]    Script Date: 2/16/2022 12:58:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ibrahim Alexis Lopez Roman
-- Create date: 15/02/2022
-- Description:	Update account.
-- =============================================
CREATE PROCEDURE [dbo].[spAccount_Update]
	@Id int,
	@AccountNumber varchar(25),
	@CurrentBalance numeric
AS
BEGIN
	
	UPDATE Account
	SET AccountNumber = @AccountNumber,
	CurrentBalance = @CurrentBalance
	OUTPUT inserted.Id
	WHERE Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[spClient_Add]    Script Date: 2/16/2022 12:58:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ibrahim Alexis Lopez Roman
-- Create date: 15/02/2022
-- Description:	Add new client.
-- =============================================
CREATE PROCEDURE [dbo].[spClient_Add]
	@Name varchar(25),
	@LastName varchar(50),
	@IdentificationNumber varchar(25)
AS
BEGIN
	INSERT INTO Clients (Name, LastName, IdentificationNumber) VAlUES(@Name, @LastName, @IdentificationNumber)

	Select @@IDENTITY as Id
END
GO
/****** Object:  StoredProcedure [dbo].[spClient_Delete]    Script Date: 2/16/2022 12:58:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ibrahim Alexis Lopez Roman
-- Create date: 15/02/2022
-- Description:	Soft delete client by id.
-- =============================================
CREATE PROCEDURE [dbo].[spClient_Delete]
	@Id int
AS
BEGIN	
	UPDATE Clients
	SET Active = 0
	OUTPUT inserted.Id
	WHERE Id = @Id

END
GO
/****** Object:  StoredProcedure [dbo].[spClient_GetAll]    Script Date: 2/16/2022 12:58:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ibrahim Alexis Lopez Roman
-- Create date: 15/02/2022
-- Description:	Get all clients.
-- =============================================
CREATE PROCEDURE [dbo].[spClient_GetAll]
	
AS
BEGIN
	SELECT * FROM Clients
	WHERE Active = 1
END
GO
/****** Object:  StoredProcedure [dbo].[spClient_GetById]    Script Date: 2/16/2022 12:58:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ibrahim Alexis Lopez Roman
-- Create date: 15/02/2022
-- Description:	Get a client by id.
-- =============================================
CREATE PROCEDURE [dbo].[spClient_GetById]
	@Id int
AS
BEGIN	
	SELECT TOP 1 *
	FROM Clients
	WHERE Id = @Id	AND Active = 1
END
GO
/****** Object:  StoredProcedure [dbo].[spClient_Update]    Script Date: 2/16/2022 12:58:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ibrahim Alexis Lopez Roman
-- Create date: 15/02/2022
-- Description:	Update a client.
-- =============================================
CREATE PROCEDURE [dbo].[spClient_Update]
	@Id int, 
	@Name varchar(25),
	@LastName varchar(50),
	@IdentificationNumber varchar(25)
AS
BEGIN
	UPDATE Clients 
	SET 
	Name =@Name, 
	LastName= @LastName, 
	IdentificationNumber = @IdentificationNumber
	OUTPUT inserted.Id
	WHERE Id = @Id
	
END
GO
/****** Object:  StoredProcedure [dbo].[spMovement_Add]    Script Date: 2/16/2022 12:58:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ibrahim Alexis Lopez Roman
-- Create date: 15/02/2022
-- Description:	Add new movement and apply balance.
-- =============================================
CREATE PROCEDURE [dbo].[spMovement_Add]
	@Amount numeric,	
	@AccountId int,
	@TypeMovementId int
AS
BEGIN	
	DECLARE @TypeValue int
	SET @TypeValue = (Select Value From TypesMovements WHERE Id = @TypeMovementId)

	UPDATE Account
	SET CurrentBalance = ( CurrentBalance + ( @Amount * @TypeValue))
	WHERE Id = @AccountId

	INSERT INTO Movements(Amount, AccountId, TypeMovementId)
	VALUES(@Amount, @AccountId, @TypeMovementId)
	SELECT @@IDENTITY AS Id 

END
GO
/****** Object:  StoredProcedure [dbo].[spMovement_GetAll]    Script Date: 2/16/2022 12:58:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ibrahim Alexis Lopez Roman
-- Create date: 15/02/2022
-- Description:	Get all movements records.
-- =============================================
CREATE PROCEDURE [dbo].[spMovement_GetAll]	
AS
BEGIN
	
	
	SELECT 
	mv.*, 
	act.*,
	cli.*
	FROM Movements mv	
    INNER JOIN (
		select * FROM (
				SELECT * FROM Account GROUP BY Id, AccountNumber,CurrentBalance, ClientId, Active
			) t GROUP BY Id, AccountNumber, CurrentBalance, ClientId, Active
	) act ON act.Id = mv.AccountId
	INNER JOIN Clients cli On cli.Id = act.ClientId
    ORDER BY mv.Id DESC
END
GO
/****** Object:  StoredProcedure [dbo].[spMovement_GetById]    Script Date: 2/16/2022 12:58:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ibrahim Alexis Lopez Roman
-- Create date: 15/02/2022
-- Description:	Get movement by Id.
-- =============================================
create PROCEDURE [dbo].[spMovement_GetById]
	@Id int
AS
BEGIN
	
	
	
	SELECT mv.*, act.* FROM Movements mv	
    INNER JOIN (
		select * FROM (
				SELECT * FROM Account GROUP BY Id, AccountNumber,CurrentBalance, ClientId, Active
			) t GROUP BY Id, AccountNumber, CurrentBalance, ClientId, Active
	) act ON act.Id = mv.AccountId
	WHERE mv.Id = @Id
    ORDER BY mv.Id DESC
END
GO
/****** Object:  StoredProcedure [dbo].[spTypeMovement_GetAll]    Script Date: 2/16/2022 12:58:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ibrahim Alexis Lopez Roman
-- Create date: 15/02/2022
-- Description:	Get all types movements.
-- =============================================
CREATE PROCEDURE [dbo].[spTypeMovement_GetAll]
AS
BEGIN
	
	SELECT * FROM TypesMovements
	
	
END
GO
