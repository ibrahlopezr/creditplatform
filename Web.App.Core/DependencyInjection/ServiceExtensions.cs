﻿using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System.Collections.Generic;
using System.Reflection;
using Web.App.Core.Mapping;
using Web.App.Core.Models;
using Web.App.Core.Services;
using Web.App.Core.Services.Interfaces;
using Web.App.Core.Validations;
using Web.App.Infrastructure.Interfaces;
using Web.App.Infrastructure.Repositories;

namespace Web.App.Core.DependencyInjection
{
    public static class ServiceExtensions
    {
        /// <summary>
        /// Repositories.
        /// </summary>
        /// <param name="repositories"></param>
        /// <returns></returns>
        public static IServiceCollection RegisterRepositories(this IServiceCollection repositories, IConfiguration configuration)
        {
            repositories.TryAddTransient<IDatabaseConnectionFactory>(e => new SqlConnectionFactory(configuration.GetConnectionString("database")));

            repositories.TryAddScoped<IClientRepository, ClientRepository>();
            repositories.TryAddScoped<IAccountRepository, AccountRepository>();
            repositories.TryAddScoped<IMovementRepository, MovementRepository>();
            repositories.TryAddScoped<ITypeMovementRepository, TypeMovementRepository>();
            return repositories;
        }

        /// <summary>
        /// Registro de Servicios.
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection RegisterServices(this IServiceCollection services)
        {
            services.TryAddTransient<IClientService, ClientService>();
            services.TryAddTransient<IAccountService, AccountService>();
            services.TryAddTransient<IMovementService, MovementService>();
            services.TryAddTransient<ITypeMovementService, TypeMovementService>();
            return services;
        }  
        
        /// <summary>
        /// Register validators.
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection RegisterValidators(this IServiceCollection services)
        {
            services.TryAddTransient<IValidator<Client>, ClientValidator>();
            services.TryAddTransient<IValidator<Account>, AccountValidator>();
            services.TryAddTransient<IValidator<Movement>, MovementValidator>();
            return services;
        }

        /// <summary>
        /// Registro de Dataservices.
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection RegisterDataServices(this IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            return services;
        }

        /// <summary>
        /// Registro de datarepositories.
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection RegisterDataRepositories(this IServiceCollection services)
        {
            services.TryAddTransient<IDataRepositories, DataRepositories>();
            return services;
        }

        /// <summary>
        /// Add Automapper dependencies
        /// </summary>
        /// <param name="services"></param>
        /// <param name="assemblies"></param>
        public static void AddDependenciesMyLibrary(this IServiceCollection services, IList<Assembly> assemblies)
        {
            assemblies.Add(Assembly.GetAssembly(typeof(AutoMapping)));
        }
    }
}
