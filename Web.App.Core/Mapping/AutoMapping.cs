﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Web.App.Core.Models;
using Web.App.Infrastructure.Entities;

namespace Web.App.Core.Mapping
{
    /// <summary>
    /// Class automapping.
    /// </summary>
    public class AutoMapping : Profile
    {
        /// <summary>
        /// Mapping models between libraries.
        /// </summary>
        public AutoMapping()
        {
            #region Normal mapping.
            CreateMap<ClientDto, Client>();
            CreateMap<AccountDto, Account>();
            CreateMap<MovementDto, Movement>();
            CreateMap<TypeMovementDto, TypeMovement>();
            #endregion

            #region Mapping reverse
            CreateMap<Client, ClientDto>();
            CreateMap<Account, AccountDto>();
            CreateMap<Movement, MovementDto>();
            CreateMap<TypeMovement, TypeMovementDto>();
            #endregion
        }
    }
}
