﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Web.App.Core.Models
{
    public class Account
    {
        public int Id { get; set; }
        [Display(Name = "Account Number")]
        public string AccountNumber { get; set; }
        [Display(Name = "Current Balance")]
        public double CurrentBalance { get; set; }
        
        
        [Display(Name = "Client")]
        public int ClientId { get; set; }
        [Display(Name = "Client")]
        public Client Client { get; set; }
        [Display(Name = "Movements")]
        public List<Movement> Movements { get; set; }
    }
}
