﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Web.App.Core.Models
{
    public class Movement
    {
        public int Id { get; set; }
        public double Amount { get; set; }
        [Display(Name = "Created At")]
        public DateTime CreatedAt { get; set; }
        [Display(Name = "Updated At")]
        public DateTime UpdatedAt { get; set; }


        public int AccountId { get; set; }
        public Account Account { get; set; }

        public int TypeMovementId { get; set; }
        public TypeMovement TypeMovement { get; set; }
    }
}
