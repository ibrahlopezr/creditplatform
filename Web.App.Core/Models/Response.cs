﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Web.App.Core.Models
{
    public class Response<T>
    {
        public string RequestId { get; set; }
        public T Data { get; set; }
        public string ErrorMsg { get; set; }
        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}
