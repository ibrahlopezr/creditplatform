﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Web.App.Core.Models
{
    public class TypeMovement
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
