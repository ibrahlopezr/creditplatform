﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Web.App.Core.Models;
using Web.App.Core.Services.Interfaces;
using Web.App.Infrastructure.Entities;
using Web.App.Infrastructure.Interfaces;

namespace Web.App.Core.Services
{
    public class AccountService : IAccountService
    {
        /// <summary>
        /// Automapper service.
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// Acoount repository
        /// </summary>
        private readonly IAccountRepository _accountRepository;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="accountRepository"></param>
        /// <param name="mapper"></param>
        public AccountService(IAccountRepository accountRepository, IMapper mapper)
        {
            _accountRepository = accountRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all accounts.
        /// </summary>
        /// <returns></returns>
        public async Task<List<Account>> GetAllAsync()
        {
            try
            {
                var accoounts = await _accountRepository.GetAllAsync();
                var result = _mapper.Map<List<Account>>(accoounts);
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Get account by identity.
        /// </summary>
        /// <returns></returns>
        public async Task<Account> GetByIdAsync(int Id)
        {
            try
            {
                var data = await _accountRepository.GetByIdAsync(Id);
                var model = _mapper.Map<Account>(data);
                return model;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Add new account.
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public async Task<int> AddAsync(Account account)
        {
            try
            {
                var model = _mapper.Map<AccountDto>(account);
                int result = await _accountRepository.AddAsync(model);
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Update an account.
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public async Task<int> UpdateAsync(Account account)
        {
            try
            {
                var model = _mapper.Map<AccountDto>(account);
                int result = await _accountRepository.UpdateAsync(model);
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Soft delete client.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<int> DeleteAsync(int Id)
        {
            try
            {
                int result = await _accountRepository.DeleteAsync(Id);
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


    }
}
