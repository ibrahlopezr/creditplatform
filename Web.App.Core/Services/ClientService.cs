﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Web.App.Core.Models;
using Web.App.Core.Services.Interfaces;
using Web.App.Infrastructure.Entities;
using Web.App.Infrastructure.Interfaces;

namespace Web.App.Core.Services
{

    /// <summary>
    /// Client Service.
    /// </summary>
    public class ClientService : IClientService
    {
        private readonly IMapper _mapper;
        private readonly IClientRepository _clientRepository;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="clientRepository"></param>
        public ClientService(IMapper mapper, IClientRepository clientRepository)
        {
            _mapper = mapper;
            _clientRepository = clientRepository;
        }

        /// <summary>
        /// Get All users.
        /// </summary>
        /// <returns></returns>
        public async Task<List<Client>> GetAllAsync()
        {
            try
            {
                var data = await _clientRepository.GetAllAsync();
                var clients = _mapper.Map<List<Client>>(data);
                return clients;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        } 
        
        /// <summary>
        /// Add new client.
        /// </summary>
        /// <returns></returns>
        public async Task<int> AddAsync(Client client)
        {
            try
            {
                var model = _mapper.Map<ClientDto>(client);
                var result = await _clientRepository.AddAsync(model);
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Update a client.
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        public async Task<int> UpdateAsync(Client client)
        {
            try
            {
                var model = _mapper.Map<ClientDto>(client);
                var result = await _clientRepository.UpdateAsync(model);
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        /// <summary>
        /// Get Client by identity.
        /// </summary>
        /// <returns></returns>
        public async Task<Client> GetByIdAsync(int Id)
        {
            try
            {
                var result = await _clientRepository.GetByIdAsync(Id);
                var client = _mapper.Map<Client>(result);
                return client;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Soft delete client.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<int> DeleteAsync(int Id)
        {
            try
            {
                int result = await _clientRepository.DeleteAsync(Id);
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}
