﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Web.App.Core.Models;

namespace Web.App.Core.Services.Interfaces
{
    public interface IAccountService
    {
        /// <summary>
        /// Get all accounts.
        /// </summary>
        /// <returns></returns>
        Task<List<Account>> GetAllAsync();

        /// <summary>
        /// Get account by identity.
        /// </summary>
        /// <returns></returns>
        Task<Account> GetByIdAsync(int Id);

        /// <summary>
        /// Add new account.
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        Task<int> AddAsync(Account account);

        /// <summary>
        /// Update an account.
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        Task<int> UpdateAsync(Account account);

        /// <summary>
        /// Soft delete client.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<int> DeleteAsync(int Id);

    }
}
