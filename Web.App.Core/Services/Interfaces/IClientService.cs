﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Web.App.Core.Models;

namespace Web.App.Core.Services.Interfaces
{
    public interface IClientService
    {
        /// <summary>
        /// Get all clients.
        /// </summary>
        /// <returns></returns>
        Task<List<Client>> GetAllAsync();

        /// <summary>
        /// Add new client.
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        Task<int> AddAsync(Client client);

        /// <summary>
        /// Update a client.
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        Task<int> UpdateAsync(Client client);

        /// <summary>
        /// Get Client by identity.
        /// </summary>
        /// <returns></returns>
        Task<Client> GetByIdAsync(int Id);

        /// <summary>
        /// Soft delete client.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<int> DeleteAsync(int Id);
    }
}
