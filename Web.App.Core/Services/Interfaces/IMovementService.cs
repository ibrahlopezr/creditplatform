﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Web.App.Core.Models;

namespace Web.App.Core.Services.Interfaces
{
    public interface IMovementService
    {
        /// <summary>
        /// Get all movements.
        /// </summary>
        /// <returns></returns>
        Task<List<Movement>> GetAllAsync();

        /// <summary>
        /// Get movement by identity.
        /// </summary>
        /// <returns></returns>
        Task<Movement> GetByIdAsync(int Id);

        /// <summary>
        /// Add movement and apply balance in an account.
        /// </summary>
        /// <param name="movement"></param>
        /// <returns></returns>
        Task<int> AddAsync(Movement movement);
    }
}
