﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Web.App.Core.Models;

namespace Web.App.Core.Services.Interfaces
{
    public interface ITypeMovementService
    {
        /// <summary>
        /// Get all types of movements. 
        /// </summary>
        /// <returns></returns>
        Task<List<TypeMovement>> GetAllAsync();
    }
}
