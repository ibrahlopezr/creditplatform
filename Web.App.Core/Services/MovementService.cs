﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Web.App.Core.Models;
using Web.App.Core.Services.Interfaces;
using Web.App.Infrastructure.Entities;
using Web.App.Infrastructure.Interfaces;

namespace Web.App.Core.Services
{
    public class MovementService : IMovementService
    {
        /// <summary>
        /// Service automapper.
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// Movement repository.
        /// </summary>
        private readonly IMovementRepository _movementRepository;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="movementRepository"></param>
        public MovementService(IMapper mapper, IMovementRepository movementRepository)
        {
            _movementRepository = movementRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all movements.
        /// </summary>
        /// <returns></returns>
        public async Task<List<Movement>> GetAllAsync()
        {
            try
            {
                var data = await _movementRepository.GetAllAsync();
                var movements = _mapper.Map<List<Movement>>(data);
                return movements;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Get movement by identity.
        /// </summary>
        /// <returns></returns>
        public async Task<Movement> GetByIdAsync(int Id)
        {
            try
            {
                var data = await _movementRepository.GetByIdAsync(Id);
                var movement= _mapper.Map<Movement>(data);
                return movement;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Add movement and apply balance in an account.
        /// </summary>
        /// <param name="movement"></param>
        /// <returns></returns>
        public async Task<int> AddAsync(Movement movement)
        {
            try
            {
                var model = _mapper.Map<MovementDto>(movement);
                var result = await _movementRepository.AddAsync(model);
                return result;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}
