﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Web.App.Core.Models;
using Web.App.Core.Services.Interfaces;
using Web.App.Infrastructure.Interfaces;

namespace Web.App.Core.Services
{
    public class TypeMovementService : ITypeMovementService
    {
        /// <summary>
        /// Automapper service.
        /// </summary>
        private readonly IMapper _mapper;
        /// <summary>
        /// Type of movements repository.
        /// </summary>
        private readonly ITypeMovementRepository _typeMovementRepository;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="typeMovementRepository"></param>
        public TypeMovementService(ITypeMovementRepository typeMovementRepository, IMapper mapper)
        {
            _typeMovementRepository = typeMovementRepository;
            _mapper = mapper;
        }


        /// <summary>
        /// Get all types of movements. 
        /// </summary>
        /// <returns></returns>
        public async Task<List<TypeMovement>> GetAllAsync()
        {
            try
            {
                var data = await _typeMovementRepository.GetAllAsync();
                var typesMovements = _mapper.Map<List<TypeMovement>>(data);
                return typesMovements;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
