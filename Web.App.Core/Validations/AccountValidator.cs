﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using Web.App.Core.Models;

namespace Web.App.Core.Validations
{
    public class AccountValidator : AbstractValidator<Account>
    {
        public AccountValidator()
        {
            RuleFor(x => x.AccountNumber).NotEmpty().NotNull().Length(1, 25);
            RuleFor(x => x.CurrentBalance).GreaterThan(0).NotEmpty();
            RuleFor(x => x.ClientId).NotEmpty();
        }
    }
}
