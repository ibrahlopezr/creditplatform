﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using Web.App.Core.Models;

namespace Web.App.Core.Validations
{
    public class ClientValidator : AbstractValidator<Client>
    {
        public ClientValidator()
        {
            RuleFor(x => x.Name).NotEmpty().NotNull().Length(1, 25);
            RuleFor(x => x.LastName).NotEmpty().NotNull().Length(1, 50);
            RuleFor(x => x.IdentificationNumber).NotEmpty().NotNull().Length(1, 25);
        }
    }
}
