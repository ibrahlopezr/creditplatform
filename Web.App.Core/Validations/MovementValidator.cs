﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using Web.App.Core.Models;

namespace Web.App.Core.Validations
{
    public class MovementValidator : AbstractValidator<Movement>
    {
        public MovementValidator()
        {
            RuleFor(x => x.TypeMovementId).NotEmpty().NotNull();
            RuleFor(x => x.Amount).NotEmpty().GreaterThan(0);
            RuleFor(x => x.AccountId).NotEmpty().NotNull();            
        }
    }
}
