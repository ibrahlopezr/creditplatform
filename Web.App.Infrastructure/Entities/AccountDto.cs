﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Web.App.Infrastructure.Entities
{
    public class AccountDto
    {
        public int Id { get; set; }
        public string AccountNumber { get; set; }
        public double CurrentBalance { get; set; }
        public int ClientId { get; set; }
        public ClientDto Client { get; set; }
        public List<MovementDto> Movements { get; set; }
    }
}
