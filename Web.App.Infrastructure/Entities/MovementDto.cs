﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Web.App.Infrastructure.Entities
{
    public class MovementDto
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public int AccountId { get; set; }
        public AccountDto Account { get; set; }

        public int TypeMovementId { get; set; }
        public TypeMovementDto TypeMovement { get; set; }
    }
}
