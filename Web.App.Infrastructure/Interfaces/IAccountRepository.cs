﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Web.App.Infrastructure.Entities;

namespace Web.App.Infrastructure.Interfaces
{
    public interface IAccountRepository
    {
        /// <summary>
        /// Get all accounts.
        /// </summary>
        /// <returns></returns>
        Task<List<AccountDto>> GetAllAsync();

        /// <summary>
        /// Get account by identity.
        /// </summary>
        /// <returns></returns>
        Task<AccountDto> GetByIdAsync(int Id);

        /// <summary>
        /// Add new account.
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        Task<int> AddAsync(AccountDto account);

        /// <summary>
        /// Update an account.
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        Task<int> UpdateAsync(AccountDto account);

        /// <summary>
        /// Soft delete client.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<int> DeleteAsync(int Id);

    }
}
