﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Web.App.Infrastructure.Entities;

namespace Web.App.Infrastructure.Interfaces
{
    /// <summary>
    /// Client Interface.
    /// </summary>
    public interface IClientRepository
    {

        /// <summary>
        /// Get all clients.
        /// </summary>
        /// <returns></returns>
        Task<List<ClientDto>> GetAllAsync();

        /// <summary>
        /// Add new client.
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        Task<int> AddAsync(ClientDto client);

        /// <summary>
        /// Update a client.
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        Task<int> UpdateAsync(ClientDto client);

        /// <summary>
        /// Get Client by identity.
        /// </summary>
        /// <returns></returns>
        Task<ClientDto> GetByIdAsync(int Id);

        /// <summary>
        /// Soft delete client.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<int> DeleteAsync(int Id);
    }
}
