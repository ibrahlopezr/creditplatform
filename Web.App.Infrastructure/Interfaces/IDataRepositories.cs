﻿using System;
using System.Collections.Generic;
using System.Text;
using Web.App.Infrastructure.Repositories;

namespace Web.App.Infrastructure.Interfaces
{
    public interface IDataRepositories
    {
        ClientRepository ClientRepository { get; set; }
        AccountRepository AccountRepository { get; set; }
        MovementRepository MovementRepository { get; set; }
        TypeMovementRepository TypeMovementRepository { get; set; }
    }
}
