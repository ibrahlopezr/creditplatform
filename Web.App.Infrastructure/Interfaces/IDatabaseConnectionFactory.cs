﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace Web.App.Infrastructure.Interfaces
{
    /// <summary>
    /// Database connection factory.
    /// </summary>
    public interface IDatabaseConnectionFactory
    {
        /// <summary>
        /// Connection.
        /// </summary>
        SqlConnection Connection { get; set; }
        /// <summary>
        /// Create Connection async function.
        /// </summary>
        /// <returns></returns>
        Task CreateConnectionAsync();
        /// <summary>
        /// Close Connection async function.
        /// </summary>
        /// <returns></returns>
        Task CloseConnectionAsync();
    }
}
