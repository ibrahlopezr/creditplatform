﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Web.App.Infrastructure.Entities;

namespace Web.App.Infrastructure.Interfaces
{
    public interface IMovementRepository
    {
        /// <summary>
        /// Get all movements.
        /// </summary>
        /// <returns></returns>
        Task<List<MovementDto>> GetAllAsync();

        /// <summary>
        /// Get movement by identity.
        /// </summary>
        /// <returns></returns>
        Task<MovementDto> GetByIdAsync(int Id);

        /// <summary>
        /// Add movement and apply balance in an account.
        /// </summary>
        /// <param name="movement"></param>
        /// <returns></returns>
        Task<int> AddAsync(MovementDto movement);
    }
}
