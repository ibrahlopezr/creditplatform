﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Web.App.Infrastructure.Entities;

namespace Web.App.Infrastructure.Interfaces
{
    public interface ITypeMovementRepository
    {
        /// <summary>
        /// Get all types of movements. 
        /// </summary>
        /// <returns></returns>
        Task<List<TypeMovementDto>> GetAllAsync();
    }
}
