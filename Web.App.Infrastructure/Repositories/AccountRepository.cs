﻿using System;
using System.Collections.Generic;
using Dapper;
using Web.App.Infrastructure.Interfaces;
using System.Threading.Tasks;
using Web.App.Infrastructure.Entities;
using System.Data;
using System.Linq;

namespace Web.App.Infrastructure.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        /// <summary>
        /// Database service.
        /// </summary>
        private readonly IDatabaseConnectionFactory _database;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="database"></param>
        public AccountRepository(IDatabaseConnectionFactory database) =>
            _database = database ?? throw new ArgumentNullException(nameof(_database));

        /// <summary>
        /// Get All Accounts
        /// </summary>
        /// <returns></returns>
        public async Task<List<AccountDto>> GetAllAsync()
        {
            try
            {
                List<AccountDto> data = null;
                var lookup = new Dictionary<int, AccountDto>();
                #region Consulta
                await _database.CreateConnectionAsync();
                //data = (await _database.Connection.QueryAsync<AccountDto>("spAccount_GetAll", commandType: CommandType.StoredProcedure)).ToList();
                data = (await _database.Connection.QueryAsync<AccountDto, ClientDto, MovementDto, AccountDto>("spAccount_GetAll",
                    (account, client, movement) =>
                    {
                        AccountDto ac = new AccountDto();
                        account.Client = client;
                        if (ac.Movements == null) ac.Movements = new List<MovementDto>();
                        if (movement != null) ac.Movements.Add(movement);
                        return account;
                    }))
                    .GroupBy(a => a.Id)
                    .Select(group => group.FirstOrDefault()).ToList();
                    

                #endregion


                return data;
            }
            catch (Exception ex)
            {

                throw new Exception($"Ocurrio un error al obtener las cuentas: {ex.Message}", ex.InnerException);
            }
            finally
            {
                // Close Connection.
                await _database.CloseConnectionAsync();
            }
        }

        /// <summary>
        /// Get account by identity.
        /// </summary>
        /// <returns></returns>
        public async Task<AccountDto> GetByIdAsync(int Id)
        {
            try
            {
                AccountDto data = null;
                var parameters = new DynamicParameters();
                parameters.Add("@Id", Id, DbType.Int32);
                #region Consulta
                await _database.CreateConnectionAsync();
                List<MovementDto> movements = null;
                //data = await _database.Connection.QueryFirstOrDefaultAsync<AccountDto>("spAccount_GetById", parameters, commandType: CommandType.StoredProcedure);
                data = (await _database.Connection.QueryAsync<AccountDto, ClientDto, MovementDto, AccountDto>($"spAccount_GetById {Id}",
                 (account, client, movement) =>
                 {
                     account.Client = client;
                     if (movements == null) movements = new List<MovementDto>();
                     if (movement != null) movements.Add(movement);
                     return account;
                 })).FirstOrDefault();
                #endregion
                data.Movements = movements;
                return data;
            }
            catch (Exception ex)
            {

                throw new Exception($"Ocurrio un error al obtener la cuenta: {ex.Message}", ex.InnerException);
            }
            finally
            {
                // Close Connection.
                await _database.CloseConnectionAsync();
            }
        }

        /// <summary>
        /// Add new account.
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public async Task<int> AddAsync(AccountDto account)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@AccountNumber", account.AccountNumber, DbType.String);
                parameters.Add("@CurrentBalance", account.CurrentBalance, DbType.String);
                parameters.Add("@ClientId", account.ClientId, DbType.Int32);
                int data = 0;
                #region Consulta
                await _database.CreateConnectionAsync();
                data = await _database.Connection.QueryFirstAsync<int>("spAccount_Add", parameters, commandType: CommandType.StoredProcedure);
                #endregion

                return data;
            }
            catch (Exception ex)
            {

                throw new Exception($"Ocurrio un error al ingresar la cuenta: {ex.Message}", ex.InnerException);
            }
            finally
            {
                await _database.CloseConnectionAsync();
            }
        }

        /// <summary>
        /// Update an account.
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public async Task<int> UpdateAsync(AccountDto account)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Id", account.Id, DbType.Int32);
                parameters.Add("@AccountNumber", account.AccountNumber, DbType.String);
                parameters.Add("@CurrentBalance", account.CurrentBalance, DbType.String);
                int data = 0;
                #region Consulta
                await _database.CreateConnectionAsync();
                data = await _database.Connection.QueryFirstAsync<int>("spAccount_Update", parameters, commandType: CommandType.StoredProcedure);
                #endregion

                return data;
            }
            catch (Exception ex)
            {

                throw new Exception($"Ocurrio un error al actualizar la cuenta: {ex.Message}", ex.InnerException);
            }
            finally
            {
                await _database.CloseConnectionAsync();

            }
        }

        /// <summary>
        /// Soft delete client.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<int> DeleteAsync(int Id)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Id", Id, DbType.Int32);
                int data = 0;
                #region Consulta
                await _database.CreateConnectionAsync();
                data = await _database.Connection.QueryFirstAsync<int>("spAccount_Delete", parameters, commandType: CommandType.StoredProcedure);
                #endregion

                return data;
            }
            catch (Exception ex)
            {

                throw new Exception($"Ocurrio un error al eliminar la cuenta: {ex.Message}", ex.InnerException);
            }
            finally
            {
                await _database.CloseConnectionAsync();

            }
        }


    }
}
