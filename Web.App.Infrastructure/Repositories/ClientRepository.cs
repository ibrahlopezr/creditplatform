﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.App.Infrastructure.Entities;
using Web.App.Infrastructure.Interfaces;

namespace Web.App.Infrastructure.Repositories
{
    public class ClientRepository : IClientRepository
    {
        /// <summary>
        /// Database service.
        /// </summary>
        private readonly IDatabaseConnectionFactory _database;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="database"></param>
        public ClientRepository(IDatabaseConnectionFactory database) =>
            _database = database ?? throw new ArgumentNullException(nameof(_database));


        /// <summary>
        /// Get All Clients
        /// </summary>
        /// <returns></returns>
        public async Task<List<ClientDto>> GetAllAsync()
        {
            try
            {
                List<ClientDto> data = null;
                #region Consulta
                await _database.CreateConnectionAsync();
                data = (await _database.Connection.QueryAsync<ClientDto>("spClient_GetAll", commandType: CommandType.StoredProcedure)).ToList();
                #endregion

                return data;
            }
            catch (Exception ex)
            {

                throw new Exception($"Ocurrio un error al obtener los clientes: {ex.Message}", ex.InnerException);
            }
            finally
            {
                // Close Connection.
                await _database.CloseConnectionAsync();
            }
        }
        
        /// <summary>
        /// Get Client by identity.
        /// </summary>
        /// <returns></returns>
        public async Task<ClientDto> GetByIdAsync(int Id)
        {
            try
            {
                ClientDto data = null;
                var parameters = new DynamicParameters();
                parameters.Add("@Id", Id, DbType.Int32);
                #region Consulta
                await _database.CreateConnectionAsync();
                data = await _database.Connection.QueryFirstOrDefaultAsync<ClientDto>("spClient_GetById", parameters, commandType: CommandType.StoredProcedure);
                #endregion

                return data;
            }
            catch (Exception ex)
            {

                throw new Exception($"Ocurrio un error al obtener el cliente: {ex.Message}", ex.InnerException);
            }
            finally
            {
                // Close Connection.
                await _database.CloseConnectionAsync();
            }
        }

        /// <summary>
        /// Add new client.
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        public async Task<int> AddAsync(ClientDto client)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Name", client.Name, DbType.String);
                parameters.Add("@LastName", client.LastName, DbType.String);
                parameters.Add("@IdentificationNumber", client.IdentificationNumber, DbType.String);
                int data = 0;
                #region Consulta
                await _database.CreateConnectionAsync();
                data = await _database.Connection.QueryFirstAsync<int>("spClient_Add", parameters, commandType: CommandType.StoredProcedure);
                #endregion

                return data;
            }
            catch (Exception ex)
            {

                throw new Exception($"Ocurrio un error al ingresar el cliente: {ex.Message}", ex.InnerException);
            }
            finally
            {
                await _database.CloseConnectionAsync();
            }
        }
        
        /// <summary>
        /// Update a client.
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        public async Task<int> UpdateAsync(ClientDto client)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Id", client.Id, DbType.Int32);
                parameters.Add("@Name", client.Name, DbType.String);
                parameters.Add("@LastName", client.LastName, DbType.String);
                parameters.Add("@IdentificationNumber", client.IdentificationNumber, DbType.String);
                int data = 0;
                #region Consulta
                await _database.CreateConnectionAsync();
                data = await _database.Connection.QueryFirstAsync<int>("spClient_Update", parameters, commandType: CommandType.StoredProcedure);
                #endregion

                return data;
            }
            catch (Exception ex)
            {

                throw new Exception($"Ocurrio un error al actualizar el cliente: {ex.Message}", ex.InnerException);
            }
            finally
            {
                await _database.CloseConnectionAsync();

            }
        } 
        
        /// <summary>
        /// Soft delete client.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<int> DeleteAsync(int Id)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Id", Id, DbType.Int32);
                int data = 0;
                #region Consulta
                await _database.CreateConnectionAsync();
                data = await _database.Connection.QueryFirstAsync<int>("spClient_Delete", parameters, commandType: CommandType.StoredProcedure);
                #endregion

                return data;
            }
            catch (Exception ex)
            {

                throw new Exception($"Ocurrio un error al actualizar el cliente: {ex.Message}", ex.InnerException);
            }
            finally
            {
                await _database.CloseConnectionAsync();

            }
        }
    
    }
}
