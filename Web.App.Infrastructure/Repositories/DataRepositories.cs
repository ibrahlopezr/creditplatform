﻿using System;
using System.Collections.Generic;
using System.Text;
using Web.App.Infrastructure.Interfaces;

namespace Web.App.Infrastructure.Repositories
{
   
    public class DataRepositories : IDataRepositories
    {
        /// <summary>
        /// Client repository.
        /// </summary>
        public ClientRepository ClientRepository { get; set; }

        /// <summary>
        /// Account repository.
        /// </summary>
        public AccountRepository AccountRepository { get; set; }

        /// <summary>
        /// Movement repository.
        /// </summary>
        public MovementRepository MovementRepository { get; set; }
        
        /// <summary>
        /// Type of movements repository.
        /// </summary>
        public TypeMovementRepository TypeMovementRepository { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="clientRepository"></param>
        /// <param name="accountRepository"></param>
        /// <param name=""></param>
        public DataRepositories(IClientRepository clientRepository, IAccountRepository accountRepository, IMovementRepository movementRepository, ITypeMovementRepository typeMovementRepository)
        {
            ClientRepository        =   (ClientRepository)clientRepository;
            AccountRepository       =   (AccountRepository)accountRepository;
            MovementRepository      =   (MovementRepository)movementRepository;
            MovementRepository      =   (MovementRepository)movementRepository;
            TypeMovementRepository  =   (TypeMovementRepository)typeMovementRepository;
        }
    }
}
