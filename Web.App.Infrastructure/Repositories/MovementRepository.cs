﻿using System;
using System.Collections.Generic;
using System.Text;
using Web.App.Infrastructure.Interfaces;
using Dapper;
using System.Data;
using System.Threading.Tasks;
using System.Linq;
using Web.App.Infrastructure.Entities;

namespace Web.App.Infrastructure.Repositories
{
    public class MovementRepository : IMovementRepository
    {

        /// <summary>
        /// Database service.
        /// </summary>
        private readonly IDatabaseConnectionFactory _database;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="database"></param>
        public MovementRepository(IDatabaseConnectionFactory database) =>
            _database = database ?? throw new ArgumentNullException(nameof(_database));

        /// <summary>
        /// Get All movements
        /// </summary>
        /// <returns></returns>
        public async Task<List<MovementDto>> GetAllAsync()
        {
            try
            {
                List<MovementDto> data = null;
                #region Consulta
                await _database.CreateConnectionAsync();
                data = (await _database.Connection.QueryAsync<MovementDto, AccountDto, ClientDto, MovementDto>("spMovement_GetAll",
                (movement, account, client) =>
                {
                    movement.Account = account;
                    movement.Account.Client = client;
                    return movement;
                })).ToList();
                #endregion

                return data;
            }
            catch (Exception ex)
            {

                throw new Exception($"Ocurrio un error al obtener los movimientos: {ex.Message}", ex.InnerException);
            }
            finally
            {
                // Close Connection.
                await _database.CloseConnectionAsync();
            }
        }

        /// <summary>
        /// Get movement by identity.
        /// </summary>
        /// <returns></returns>
        public async Task<MovementDto> GetByIdAsync(int Id)
        {
            try
            {
                MovementDto data = null;
                var parameters = new DynamicParameters();
                parameters.Add("@Id", Id, DbType.Int32);
                #region Consulta
                await _database.CreateConnectionAsync();
                data = await _database.Connection.QueryFirstOrDefaultAsync<MovementDto>("spMovement_GetById", parameters, commandType: CommandType.StoredProcedure);
                #endregion

                return data;
            }
            catch (Exception ex)
            {

                throw new Exception($"Ocurrio un error al obtener el movimiento: {ex.Message}", ex.InnerException);
            }
            finally
            {
                // Close Connection.
                await _database.CloseConnectionAsync();
            }
        }

        /// <summary>
        /// Add movement and apply balance in an account.
        /// </summary>
        /// <param name="movement"></param>
        /// <returns></returns>
        public async Task<int> AddAsync(MovementDto movement)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Amount", movement.Amount, DbType.Decimal);
                parameters.Add("@AccountId", movement.AccountId, DbType.Int32);
                parameters.Add("@TypeMovementId", movement.TypeMovementId, DbType.Int32);
                int data = 0;
                #region Consulta
                await _database.CreateConnectionAsync();
                data = await _database.Connection.QueryFirstAsync<int>("spMovement_Add", parameters, commandType: CommandType.StoredProcedure);
                #endregion

                return data;
            }
            catch (Exception ex)
            {

                throw new Exception($"Ocurrio un error al ingresar el movimiento: {ex.Message}", ex.InnerException);
            }
            finally
            {
                await _database.CloseConnectionAsync();
            }
        }
    }
}
