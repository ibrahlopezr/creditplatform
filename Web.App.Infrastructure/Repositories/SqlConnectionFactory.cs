﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using Web.App.Infrastructure.Interfaces;

namespace Web.App.Infrastructure.Repositories
{
    public class SqlConnectionFactory : IDatabaseConnectionFactory
    {
        /// <summary>
        /// Connection string.
        /// </summary>
        private readonly string _connectionString;

        /// <summary>
        /// Connection.
        /// </summary>
        public SqlConnection Connection { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="connectionString"></param>
        public SqlConnectionFactory(string connectionString) => _connectionString = connectionString ??
            throw new ArgumentException(nameof(connectionString));


        /// <summary>
        /// Open connection with database;
        /// </summary>
        /// <returns></returns>
        public async Task CreateConnectionAsync()
        {
            Connection = new SqlConnection(_connectionString);
            await Connection.OpenAsync();
            //return Connection;
        }

        /// <summary>
        /// Close connection with database.
        /// </summary>
        /// <returns></returns>
        public async Task CloseConnectionAsync()
        {
            await Connection.CloseAsync();
            Connection = null;
        }
    }
}
