﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.App.Infrastructure.Entities;
using Web.App.Infrastructure.Interfaces;

namespace Web.App.Infrastructure.Repositories
{
    public class TypeMovementRepository : ITypeMovementRepository
    {
        /// <summary>
        /// Database service.
        /// </summary>
        private readonly IDatabaseConnectionFactory _database;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="database"></param>
        public TypeMovementRepository(IDatabaseConnectionFactory database) =>
            _database = database ?? throw new ArgumentNullException(nameof(_database));


        /// <summary>
        /// Get all types of movements. 
        /// </summary>
        /// <returns></returns>
        public async Task<List<TypeMovementDto>> GetAllAsync()
        {
            try
            {
                List<TypeMovementDto> data = null;
                #region Consulta
                await _database.CreateConnectionAsync();
                data = (await _database.Connection.QueryAsync<TypeMovementDto>("spTypeMovement_GetAll", commandType: CommandType.StoredProcedure)).ToList();
                #endregion

                return data;
            }
            catch (Exception ex)
            {

                throw new Exception($"Ocurrio un error al obtener los tipos de movimientos: {ex.Message}", ex.InnerException);
            }
            finally
            {
                // Close Connection.
                await _database.CloseConnectionAsync();
            }
        }
    }
}
