﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.App.Core.Models;
using Web.App.Core.Services.Interfaces;

namespace Web.App.Controllers
{
    public class AccountsController : Controller
    {

        /// <summary>
        /// Account service.
        /// </summary>
        private readonly IAccountService _accountService;

        /// <summary>
        /// Client service.
        /// </summary>
        private readonly IClientService _clientService;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="accountService"></param>
        /// <param name="clientService"></param>
        public AccountsController(IAccountService accountService, IClientService clientService)
        {
            _accountService = accountService;
            _clientService = clientService;
        }

        /// <summary>
        /// Get accounts.        
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            try
            {
                List<Account> accounts = await _accountService.GetAllAsync();

                return View(accounts);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// View add new accounts.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Add()
        {
            ViewBag.Clients =  new SelectList(await _clientService.GetAllAsync(), "Id", "FullName");
            return View();
        }

        /// <summary>
        /// Add new account.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Add(Account model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int id = await _accountService.AddAsync(model);
                    if (id > 0) return RedirectToAction(nameof(Index)); 
                    else return View(model);                    
                }

                return View(model);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        /// <summary>
        /// Get client by id.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Update(int Id)
        {
            try
            {
                Account model = await _accountService.GetByIdAsync(Id);
                if (model == null)
                {
                    return NotFound();
                }
                ViewBag.Clients = new SelectList(await _clientService.GetAllAsync(), "Id", "FullName");
                return View(model);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }


        /// <summary>
        /// Update Account.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Update(Account model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int id = await _accountService.UpdateAsync(model);
                    if (id > 0) return RedirectToAction(nameof(Index));
                    else return View(model);
                }
                return View(model);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public async Task<IActionResult> Transactions(int Id)
        {
            try
            {
                Account model = await _accountService.GetByIdAsync(Id);
                if (model == null)
                {
                    return NotFound();
                }

                return View(model);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}
