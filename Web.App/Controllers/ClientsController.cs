﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Web.App.Core.Models;
using Web.App.Core.Services.Interfaces;

namespace CreditPlatform.Controllers
{
    public class ClientsController : Controller
    {
        /// <summary>
        /// Logger service.
        /// </summary>
        private readonly ILogger<ClientsController> _logger;
        /// <summary>
        /// Client service.
        /// </summary>
        private readonly IClientService _clientService;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="clientService"></param>
        public ClientsController(ILogger<ClientsController> logger, IClientService clientService)
        {
            _logger = logger;
            _clientService = clientService;
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            try
            {
                List<Client> clients = await _clientService.GetAllAsync();
                return View(clients);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// View add new user.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        /// <summary>
        /// Add new user.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Add(Client model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int id = await _clientService.AddAsync(model);
                    if (id > 0)
                        return RedirectToAction(nameof(Index));
                    else
                        return View(model);
                }

                return View(model);


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Get client by id.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Update(int Id)
        {
            try
            {
                Client model = await _clientService.GetByIdAsync(Id);
                if (model == null)
                {
                    return NotFound();
                }

                return View(model);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
        
        
        /// <summary>
        /// Get client by id.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Update(Client model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int id = await _clientService.UpdateAsync(model);
                    if (id > 0)
                        return RedirectToAction(nameof(Index));
                    else
                        return View(model);
                }
                return View(model);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }


        /// <summary>
        /// Delete client - GET.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Delete(int Id)
        {
            try
            {
                Client client = await _clientService.GetByIdAsync(Id);
                if (client == null)
                {
                    return NotFound();
                }

                return View(client);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Delete client - POST
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("Delete")]
        public async Task<IActionResult> DeletePost(int Id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int id = await _clientService.DeleteAsync(Id);
                    if (id > 0)
                        return RedirectToAction(nameof(Index));
                    else
                        return BadRequest();
                }
                return View();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }



        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(Activity.Current?.Id ?? HttpContext.TraceIdentifier);
        }
    }
}
