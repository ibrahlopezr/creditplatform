﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.App.Core.Models;
using Web.App.Core.Services.Interfaces;

namespace Web.App.Controllers
{
    public class MovementsController : Controller
    {
        /// <summary>
        /// Movements service.
        /// </summary>
        private readonly IMovementService _movementService;

        /// <summary>
        /// Account service.
        /// </summary>
        private readonly IAccountService _accountService;

        /// <summary>
        /// Types of movements service.
        /// </summary>
        private readonly ITypeMovementService _typeMovementService;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="movementRepository"></param>
        /// <param name="accountService"></param>
        public MovementsController(IMovementService movementRepository, IAccountService accountService, ITypeMovementService typeMovementService)
        {
            _movementService = movementRepository;
            _accountService = accountService;
            _typeMovementService = typeMovementService;
        }

        /// <summary>
        /// Get all movements.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            List<Movement> movements = await _movementService.GetAllAsync();
            return View(movements);
        }

        /// <summary>
        /// Add new movements to account.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Add(int Id)
        {
            try
            {
                var account = await _accountService.GetByIdAsync(Id);

                if (account == null)
                {
                    return NotFound();
                }

                ViewBag.TypesMovements = new SelectList(await _typeMovementService.GetAllAsync(), "Id", "Name");
                return View(new Movement() { AccountId = account.Id, Account = account });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Add new movement.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Add(Movement model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ViewBag.TypesMovements = new SelectList(await _typeMovementService.GetAllAsync(), "Id", "Name");
                    int id = 0;
                    var account = await _accountService.GetByIdAsync(model.Id);
                    if (model.TypeMovementId == 1)
                    {
                        if (model.Amount > account.CurrentBalance)
                        {
                            ModelState.AddModelError("Amount", $"The amount is greater than the balance ({account.CurrentBalance:C}).");
                            return View(model);
                        }
                    }
                    // Add movement.
                    id = await _movementService.AddAsync(model);
                    // Redirect to action.
                    if (id > 0) return RedirectToAction(nameof(AccountsController.Transactions), "Accounts",new { Id = model.Id });
                    else return View(model);

                }

                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(model);
            }
        }

    }
}
