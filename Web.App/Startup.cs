using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Web.App.Core.DependencyInjection;
using Web.App.Core.Helpers;

namespace CreditPlatform
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            InitialConfigurationAutoMapper(services);
            #region Cors
            services.AddCors(o =>
                o.AddPolicy(PolicyCors._AllowOrigin, builder => {
                    builder
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowAnyOrigin();
                }));

            #endregion

            #region ServiceExentions            
            services.RegisterRepositories(Configuration);
            services.RegisterServices();
            services.RegisterDataRepositories();
            services.RegisterDataServices();
            services.RegisterValidators();
            #endregion

            services.AddControllersWithViews()
            .AddFluentValidation()
            .AddJsonOptions(opts => opts.JsonSerializerOptions.PropertyNamingPolicy = null)
            .AddNewtonsoftJson(x =>
                x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            #region Cors 
            app.UseCors(PolicyCors._AllowOrigin);
            #endregion

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Clients}/{action=Index}/{id?}");
            });
        }

        #region Custom methods.
        private static void InitialConfigurationAutoMapper(IServiceCollection services)
        {
            var assemblies = new List<Assembly>();
            // register DI and add to the assemblies collection
            services.AddDependenciesMyLibrary(assemblies);
            // add the apps local automapper config 
            assemblies.Add(Assembly.GetAssembly(typeof(Startup)));
            // Initialise AutoMapper
            services.AddAutoMapper(assemblies);
        }
        #endregion
    }
}
